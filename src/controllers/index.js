module.exports.authController = require("./auth.controller");
module.exports.userController = require("./user.controller");
module.exports.productController = require("./product.controller");
module.exports.customerController = require("./customer.controller");
module.exports.contractController = require("./contract.controller");
module.exports.reservationController = require("./reservation.controller");
