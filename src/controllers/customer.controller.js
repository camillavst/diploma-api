const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { customerService } = require("../services");

const getCustomers = catchAsync(async (req, res) => {
  const customers = await customerService.getCustomers(req.query, req.user);
  const response = await Promise.all(customers.map(async customer => customer.serialize()));
  res.send(response);
});

const getCustomer = catchAsync(async (req, res) => {
  const customer = await customerService.getCustomerById(req.params.customerId, req.user);
  res.send(await customer.serialize());
});

const updateCustomer = catchAsync(async (req, res) => {
  const customer = await customerService.updateCustomer(req.params.customerId, req.body, req.user);
  res.send(await customer.serialize());
});

const createCustomer = catchAsync(async (req, res) => {
  const prod = await customerService.createCustomer(req.body, req.user);
  res.status(httpStatus.CREATED).send(await prod.serialize());
});

const deleteCustomer = catchAsync(async (req, res) => {
  await customerService.deleteCustomer(req.params.customerId, req.user);
  res.status(httpStatus.NO_CONTENT).send();
});

const getAvailableForUser = catchAsync(async (req, res) => {
  const customers = await customerService.getAvailableForUser(req.params.userId);
  const response = await Promise.all(customers.map(async customer => customer.serialize()));
  res.send(response);
});

module.exports = {
  getCustomers,
  getCustomer,
  updateCustomer,
  createCustomer,
  deleteCustomer,
  getAvailableForUser,
};
