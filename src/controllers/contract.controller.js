const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { contractService } = require("../services");
const encodeURL = require("../utils/encodeRFCUrl");

const getContracts = catchAsync(async (req, res) => {
  const contracts = await contractService.getContracts(req.query, req.user);
  const response = await Promise.all(contracts.map(async contract => contract.serialize()));
  res.send(response);
});

const getContract = catchAsync(async (req, res) => {
  const contract = await contractService.getContractById(req.params.contractId, req.user);
  res.send(await contract.serialize());
});

const updateContract = catchAsync(async (req, res) => {
  const contract = await contractService.updateContract(req.params.contractId, req.body, req.user);
  res.send(await contract.serialize());
});

const createContract = catchAsync(async (req, res) => {
  const contract = await contractService.createContract(req.body, req.user);
  res.status(httpStatus.CREATED).send(await contract.serialize());
});

const deleteContract = catchAsync(async (req, res) => {
  await contractService.deleteContract(req.params.contractId, req.user);
  res.status(httpStatus.NO_CONTENT).send();
});

const printContract = catchAsync(async (req, res) => {
  const { contractId } = req.params;
  const contractFileName = `${encodeURL(contractId)}.pdf`;
  const contractDataStream = await contractService.printContract(contractId, req.user);

  res.setHeader("Content-disposition", `inline; filename="${contractFileName}"`);
  // res.setHeader("Content-disposition", `attachment filename="${contractFileName}"`);
  res.setHeader("Content-type", "application/pdf");

  contractDataStream.pipe(res);
});

const getAvailableForUser = catchAsync(async (req, res) => {
  const contracts = await contractService.getAvailableForUser(req.params.userId, req.user);
  const response = await Promise.all(contracts.map(async contract => contract.serialize()));
  res.send(response);
});

module.exports = {
  getContracts,
  getContract,
  createContract,
  updateContract,
  deleteContract,
  printContract,
  getAvailableForUser
};
