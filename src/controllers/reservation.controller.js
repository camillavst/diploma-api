const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { reservationService } = require("../services");

const getReservations = catchAsync(async (req, res) => {
  const reservations = await reservationService.getReservations(req.query, req.user);
  const response = await Promise.all(reservations.map(async reservation => reservation.serialize()));
  res.send(response);
});

const getReservation = catchAsync(async (req, res) => {
  const reservation = await reservationService.getReservationById(req.params.reservationId, req.user);
  res.send(await reservation.serialize());
});

const updateReservation = catchAsync(async (req, res) => {
  const reservation = await reservationService.updateReservation(req.params.reservationId, req.body, req.user);
  res.send(await reservation.serialize());
});

const createReservation = catchAsync(async (req, res) => {
  const reservation = await reservationService.createReservation(req.body, req.user);
  res.status(httpStatus.CREATED).send(await reservation.serialize());
});

const deleteReservation = catchAsync(async (req, res) => {
  await reservationService.deleteReservation(req.params.reservationId, req.user);
  res.status(httpStatus.NO_CONTENT).send();
});

const getAvailable = catchAsync(async (req, res) => {
  const reservations = await reservationService.getAvailable(req.user._id);
  const response = await Promise.all(reservations.map(async reservation => reservation.serialize()));
  res.send(response);
});

const getAvailableForUser = catchAsync(async (req, res) => {
  const reservations = await reservationService.getAvailable(req.params.userId);
  const response = await Promise.all(reservations.map(async reservation => reservation.serialize()));
  res.send(response);
});

module.exports = {
  getReservations,
  getReservation,
  createReservation,
  updateReservation,
  deleteReservation,
  getAvailable,
  getAvailableForUser,
};
