const httpStatus = require("http-status");
const catchAsync = require("../utils/catchAsync");
const { productService } = require("../services");

const getProducts = catchAsync(async (req, res) => {
  const products = await productService.getProducts(req.query);
  const response = await Promise.all(products.map(product => product.serialize()));
  res.send(response);
});

const getProduct = catchAsync(async (req, res) => {
  const prod = await productService.getProductById(req.params.productId);
  res.send(await prod.serialize());
});

const updateProduct = catchAsync(async (req, res) => {
  const product = await productService.updateProduct(req.params.productId, req.body);
  res.send(await product.serialize());
});

const createProduct = catchAsync(async (req, res) => {
  const prod = await productService.createProduct(req.body);
  res.status(httpStatus.CREATED).send(await prod.serialize());
});

const deleteProduct = catchAsync(async (req, res) => {
  await productService.deleteProduct(req.params.productId);
  res.status(httpStatus.NO_CONTENT).send();
});

const getAvailable = catchAsync(async (req, res) => {
  const products = await productService.getAvailable(req.user._id);
  const response = await Promise.all(products.map(product => product.serialize()));
  res.send(response);
});

const getAvailableForUser = catchAsync(async (req, res) => {
  const products = await productService.getAvailable(req.params.userId);
  const response = await Promise.all(products.map(product => product.serialize()));
  res.send(response);
});

module.exports = {
  getProducts,
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  getAvailable,
  getAvailableForUser,
};
