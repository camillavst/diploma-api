const mongoose = require("mongoose");
const { pick } = require("lodash");
const { User, Reservation, Customer } = require(".");
/*
  Поля для договора
  Номер договора (string)
  Дата договора (datetime)
  Наименование контрагента(string)
  Перечень оборудования выходит вместе с ценой продажи товара если цена продажи не заполнена должны быть возможность заполнить. (добавлять оборудование из списка) (список?)
  Условия оплаты (string)
  Условия поставки(string)
 */

const contractSchema = mongoose.Schema(
  {
    number: {
      type: String,
      required: true,
      trim: true,
    },
    date: {
      type: Date,
      required: true,
    },
    contractor: {
      type: String,
      required: true,
      trim: true,
    },
    paymentConditions: {
      type: String,
      required: true,
      trim: true,
    },
    deliveryConditions: {
      type: String,
      required: true,
      trim: true,
    },
    reservationIds: {
      type: [mongoose.SchemaTypes.ObjectId],
      ref: "Reservation",
      required: true,
    },
    customerId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Customer",
      required: true,
    },
    managerId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  }
);

contractSchema.methods.serialize = async function() {
  const contract = this;
  const contractObj = contract.toJSON();

  if (contract.managerId) {
    const manager = await User.findOne(contract.managerId);
    if (manager) {
      contractObj.manager = await manager.serialize();
    }
  }
  if (contract.customerId) {
    const customer = await Customer.findOne(contract.customerId);
    if (customer) {
      contractObj.customer = await customer.serialize();
    }
  }
  if (contract.reservationIds && contract.reservationIds.length > 0) {
    const reservations = await Reservation.find({ _id: { $in: contract.reservationIds } });
    if (reservations.length > 0) {
      contractObj.reservations = await Promise.all(reservations.map(async res => res.serialize()));
    }
  }

  return pick(contractObj, [
    "id",
    "number",
    "date",
    "contractor",
    "paymentConditions",
    "deliveryConditions",
    "reservationIds",
    "reservations",
    "customerId",
    "customer",
    "managerId",
    "manager",
  ]);
};

const Contract = mongoose.model("Contract", contractSchema);

module.exports = Contract;
