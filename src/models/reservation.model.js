const mongoose = require("mongoose");
const { pick } = require("lodash");
const { User, Product } = require(".");
/*
  Наименования товара выходит вместе с ценой продажи товара если цена продажи не заполнена должна быть возможность заполнить. (добавлять оборудование из списка)
  Количество товара ()
  Дата до какого числа бронь (datetime)
  Менеджер, оставивший бронь
  Примечание (string).
*/

const reservationSchema = mongoose.Schema(
  {
    productId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Product",
      required: true,
    },
    amount: {
      type: Number,
      required: true,
      validate(value) {
        if (value < 1) {
          throw new Error("amount number must be 1 or greater");
        }
        if (!Number.isInteger(value)) {
          throw new Error("amount number must be integer");
        }
      },
    },
    dueDate: {
      type: Date,
      required: true,
    },
    notes: {
      type: String,
      trim: true,
    },
    contractId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "Contract",
      default: null,
    },

    managerId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  }
);

reservationSchema.methods.serialize = async function() {
  const reservation = this;
  let manager;
  const reservationObj = reservation.toJSON();
  if (reservation.managerId) {
    manager = await User.findOne(reservationObj.managerId);
    if (manager) {
      reservationObj.manager = await manager.serialize();
    }
  }
  if (reservation.productId) {
    const product = await Product.findOne(reservation.productId);
    if (product) {
      reservationObj.product = await product.serialize();
    }
  }
  return pick(reservationObj, ["id", "productId", "product", "amount", "dueDate", "notes", "managerId", "manager"]);
};

const Reservation = mongoose.model("Reservation", reservationSchema);

module.exports = Reservation;
