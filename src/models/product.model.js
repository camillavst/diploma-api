const mongoose = require("mongoose");
const { pick } = require("lodash");

/*
  Поля для товаров
  a. Наименование товара (string)
  b. Категория товара из только три (реагенты, расходные материалы, оборудование)
  c. Серия товара (просто числа от 1 до бесконечности) (int)
  d. Цена (double)
  e. Цена продажи (double)
  f. Доступное количество (высчитываемое, меняется в после того как товар законтрактуют т.е создадут)
  g. Годен до (дата; возможность указать что его нет.) (datetime)
*/

const productCategory = ["reagents", "consumables", "equipment"];

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    category: {
      type: String,
      required: true,
      enum: productCategory,
    },
    supplyNumber: {
      type: Number,
      required: true,
      validate(value) {
        if (value < 1) {
          throw new Error("Supply number must be 1 or greater");
        }
        if (!Number.isInteger(value)) {
          throw new Error("Supply number must be integer");
        }
      },
    },
    buyPrice: {
      type: Number,
      required: true,
      validate(value) {
        if (value < 0) {
          throw new Error("Price must be 0 or greater");
        }
      },
    },
    sellPrice: {
      type: Number,
      validate(value) {
        if (value < 0) {
          throw new Error("Price must be 0 or greater");
        }
      },
    },
    buyAmount: {
      type: Number,
      required: true,
      validate(value) {
        if (value < 0) {
          throw new Error("Amount must be 0 or greater");
        }
        if (!Number.isInteger(value)) {
          throw new Error("Amount must be integer");
        }
      },
    },
    currentAmount: {
      type: Number,
      required: true,
      validate(value) {
        if (value < 0) {
          throw new Error("Amount must be 0 or greater");
        }
        if (!Number.isInteger(value)) {
          throw new Error("Amount must be integer");
        }
      },
    },
    validUntil: {
      type: Date,
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  }
);

productSchema.methods.serialize = async function() {
  const product = this;
  const productObj = product.toJSON();

  return pick(productObj, [
    "id",
    "name",
    "category",
    "supplyNumber",
    "buyPrice",
    "sellPrice",
    "buyAmount",
    "currentAmount",
    "validUntil",
    "managerId",
    "manager",
  ]);
};

const Product = mongoose.model("Product", productSchema);

module.exports = Product;
