module.exports.Token = require("./token.model");
module.exports.User = require("./user.model");
module.exports.Product = require("./product.model");
module.exports.Customer = require("./customer.model");
module.exports.Reservation = require("./reservation.model");
module.exports.Contract = require("./contract.model");
