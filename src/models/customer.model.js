const mongoose = require("mongoose");
const { pick } = require("lodash");
const { User } = require(".");
/*
    Наименование организации (string)
    ИНН (int)
    Адрес (string)
    Телефон (string)
    Рассчетный счет (int)
    Банк (string)
    МФО (int)
    ОКЭД (int)
    Код плательщика НДС (int)
    Директор(string)
    Менеджер клиента (у каждого только сове имя в выпадающем списке, у админа все имена)
 */

const customerSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    address: {
      type: String,
      required: true,
      trim: true,
    },
    phone: {
      type: String,
      required: true,
      trim: true,
    },
    directorFullName: {
      type: String,
      required: true,
      trim: true,
    },
    bankAccount: {
      type: String,
      required: true,
      trim: true,
    },
    bankName: {
      type: String,
      required: true,
      trim: true,
    },
    inn: {
      type: String,
      required: true,
      trim: true,
    },
    mfo: {
      type: String,
      required: true,
      trim: true,
    },
    oked: {
      type: String,
      required: true,
      trim: true,
    },
    ndsPayerCode: {
      type: String,
      required: true,
      trim: true,
    },
    managerId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: "User",
      required: true,
    },
  },
  {
    timestamps: true,
    toObject: { getters: true },
    toJSON: { getters: true },
  }
);

customerSchema.methods.serialize = async function() {
  const customer = this;
  let manager;
  const customerObj = customer.toJSON();
  if (customer.managerId) {
    manager = await User.findOne(customer.managerId);
    if (manager) {
      customerObj.manager = await manager.serialize();
    }
  }
  return pick(customerObj, [
    "id",
    "name",
    "address",
    "phone",
    "directorFullName",
    "deliveryConditions",
    "bankAccount",
    "bankName",
    "inn",
    "mfo",
    "oked",
    "ndsPayerCode",
    "managerId",
    "manager",
  ]);
};

const Customer = mongoose.model("Customer", customerSchema);

module.exports = Customer;
