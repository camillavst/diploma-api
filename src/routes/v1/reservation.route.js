const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { reservationValidation } = require("../../validations");
const { reservationController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .get(auth("commonUser"), reservationController.getReservations)
  .post(auth("commonUser"), reservationController.createReservation);

router.get("/available", auth("commonUser"), reservationController.getAvailable);
router.get("/available/:userId", auth("manageUsers"), reservationController.getAvailableForUser);

router
  .route("/:reservationId")
  .get(auth("commonUser"), reservationController.getReservation)
  .put(auth("commonUser"), reservationController.updateReservation)
  .delete(auth("commonUser"), reservationController.deleteReservation);

module.exports = router;
