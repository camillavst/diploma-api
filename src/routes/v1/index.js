const express = require("express");
const authRoute = require("./auth.route");
const userRoute = require("./user.route");
const productRoute = require("./product.route");
const customerRoute = require("./customer.route");
const contractRoute = require("./contract.route");
const reservationRoute = require("./reservation.route");

const router = express.Router();

router.use("/auth", authRoute);
router.use("/users", userRoute);
router.use("/products", productRoute);
router.use("/customers", customerRoute);
router.use("/contracts", contractRoute);
router.use("/reservations", reservationRoute);

module.exports = router;
