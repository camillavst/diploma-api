const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { customerValidation } = require("../../validations");
const { customerController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .get(auth("commonUser"), customerController.getCustomers)
  .post(auth("commonUser"), customerController.createCustomer);

router.get("/available/:userId", auth("manageUsers"), customerController.getAvailableForUser);

router
  .route("/:customerId")
  .get(auth("commonUser"), customerController.getCustomer)
  .put(auth("commonUser"), customerController.updateCustomer)
  .delete(auth("commonUser"), customerController.deleteCustomer);

module.exports = router;
