const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { contractValidation } = require("../../validations");
const { contractController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .get(auth("commonUser"), contractController.getContracts)
  .post(auth("commonUser"), contractController.createContract);

router.get("/available/:userId", auth("manageUsers"), contractController.getAvailableForUser);

router
  .route("/:contractId")
  .get(auth("commonUser"), contractController.getContract)
  .put(auth("commonUser"), contractController.updateContract)
  .delete(auth("commonUser"), contractController.deleteContract);

router.get("/:contractId/print", auth("commonUser"), contractController.printContract);

module.exports = router;
