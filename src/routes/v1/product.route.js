const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { productValidation } = require("../../validations");
const { productController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .get(auth("commonUser"), productController.getProducts)
  .post(auth("commonUser"), validate(productValidation.createProduct), productController.createProduct);

router.get("/available", auth("commonUser"), productController.getAvailable);
router.get("/available/:userId", auth("manageUsers"), productController.getAvailableForUser);

router
  .route("/:productId")
  .get(auth("commonUser"), productController.getProduct)
  .put(auth("commonUser"), productController.updateProduct)
  .delete(auth("commonUser"), productController.deleteProduct);

module.exports = router;
