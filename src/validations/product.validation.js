const Joi = require("@hapi/joi");
const { objectId } = require("./custom.validation");

const createProduct = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    category: Joi.string()
      .required()
      .valid("reagents", "consumables", "equipment"),
    supplyNumber: Joi.number()
      .required()
      .integer()
      .min(1)
      .max(999999999999),
    buyPrice: Joi.number()
      .required()
      .min(0)
      .max(999999999999),
    sellPrice: Joi.number()
      .required()
      .min(0)
      .max(999999999999),
    buyAmount: Joi.number()
      .required()
      .min(0)
      .max(999999999999),
    currentAmount: Joi.number()
      .required()
      .min(0)
      .max(999999999999),
    validUntil: Joi.date().iso(),
  }),
};

module.exports = {
  createProduct,
};
