module.exports.authValidation = require("./auth.validation");
module.exports.userValidation = require("./user.validation");
module.exports.productValidation = require("./product.validation");
module.exports.contractValidation = require("./contract.validation");
module.exports.customerValidation = require("./customer.validation");
module.exports.reservationValidation = require("./reservation.validation");
