module.exports.authService = require("./auth.service");
module.exports.emailService = require("./email.service");
module.exports.tokenService = require("./token.service");
module.exports.userService = require("./user.service");
module.exports.productService = require("./product.service");
module.exports.customerService = require("./customer.service");
module.exports.contractService = require("./contract.service");
module.exports.reservationService = require("./reservation.service");
