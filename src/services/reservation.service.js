const httpStatus = require("http-status");
const { pick } = require("lodash");
const AppError = require("../utils/AppError");
const { Reservation, Contract } = require("../models");
const { productService } = require(".");
const { getQueryOptions } = require("../utils/service.util");

const getReservations = async (query, user) => {
  const filter = pick(query, ["managerId"]);
  if (user && user.role === "user") {
    filter.managerId = user._id;
  }
  const options = getQueryOptions(query);
  const reservations = await Reservation.find(filter, null, options);
  return reservations;
};

const getReservationById = async (reservationId, user) => {
  const filter = { _id: reservationId };
  if (user.role === "user") {
    filter.managerId = user._id;
  }
  const reservation = await Reservation.findOne(filter);
  if (!reservation) {
    throw new AppError(httpStatus.NOT_FOUND, "Reservation not found");
  }
  return reservation;
};

const reserve = async reservationBody => {
  await productService.reserve(reservationBody.productId, reservationBody.amount);
  const reservation = await Reservation.create(reservationBody);
  return reservation;
};

const createReservation = async (reservationBody, user) => {
  const reservationObj = reservationBody;
  if (user && user._id) {
    reservationObj.managerId = user._id.toString();
  }
  if (await productService.canBeReserved(reservationObj.productId, reservationObj.amount)) {
    const reservation = await reserve(reservationObj);
    return reservation;
  }
  throw new AppError(httpStatus.CONFLICT, "Can't create reservation");
};

const updateReservation = async (reservationId, reservationBody, user) => {
  const reservation = await getReservationById(reservationId, user);
  // TODO check destructive here;
  const oldReservation = new Reservation(reservation);
  Object.assign(reservation, reservationBody);

  if (!reservation.productId.equals(oldReservation.productId)) {
    throw new AppError(httpStatus.CONFLICT, "Can't update reservation. productId can't be changed");
  }
  await reservation.save();

  if (reservation.amount !== oldReservation.amount) {
    await productService.unreserv(reservation.productId, oldReservation.amount);
    await productService.reserve(reservation.productId, reservation.amount);
  }

  return reservation;
};

const deleteReservation = async (reservationId, user) => {
  const reservation = await getReservationById(reservationId, user);
  const contracts = await Contract.find({ reservationIds: reservation._id });
  const contractIds = contracts.map(c => c._id);
  if (contracts.length !== 0) {
    throw new AppError(httpStatus.CONFLICT, "Reservation used in contracts. Can't delete", { contractIds });
  }
  // we can delete reservation. Before it, we need to add product amount;
  await productService.unreserv(reservation.productId, reservation.amount);
  await reservation.remove();
  return reservation;
};

const getAvailable = async userId => {
  const reservations = await Reservation.find({ contractId: { $eq: null }, managerId: { $eq: userId } });
  return reservations;
};

module.exports = {
  getReservations,
  getReservationById,
  createReservation,
  updateReservation,
  deleteReservation,
  getAvailable
};
