const httpStatus = require("http-status");
const { pick } = require("lodash");
const AppError = require("../utils/AppError");
const { Customer, Contract } = require("../models");
const { getQueryOptions } = require("../utils/service.util");

const getCustomers = async (query, user) => {
  const filter = pick(query, ["name", "inn", "oked"]);
  if (user.role === "user") {
    filter.managerId = user._id;
  }
  const options = getQueryOptions(query);
  const customers = await Customer.find(filter, null, options);
  return customers;
};

const getCustomerById = async (customerId, user) => {
  const filter = { _id: customerId };
  if (user.role === "user") {
    filter.managerId = user._id;
  }
  const customer = await Customer.findOne(filter);
  if (!customer) {
    throw new AppError(httpStatus.NOT_FOUND, "Customer not found");
  }
  return customer;
};

const createCustomer = async (customerBody, user) => {
  const customerObj = customerBody;
  if (user && user._id) {
    customerObj.managerId = user._id.toString();
  }
  const customer = await Customer.create(customerObj);
  return customer;
};

const updateCustomer = async (customerId, customerBody, user) => {
  const customer = await getCustomerById(customerId, user);
  Object.assign(customer, customerBody);
  await customer.save();
  return customer;
};

const deleteCustomer = async (customerId, user) => {
  const customer = await getCustomerById(customerId, user);
  const contracts = await Contract.find({ customerId });
  const contractIds = contracts.map(c => c._id);
  if (contracts.length !== 0) {
    throw new AppError(httpStatus.CONFLICT, "Customer used in contracts. Can't delete", { contractIds });
  }
  await customer.remove();
  return customer;
};

const getAvailableForUser = async (requestedUserId) => {
  const contracts = Contract.find({ managerId: requestedUserId });
  return contracts;
};

module.exports = {
  getCustomers,
  getCustomerById,
  createCustomer,
  updateCustomer,
  deleteCustomer,
  getAvailableForUser,
};
