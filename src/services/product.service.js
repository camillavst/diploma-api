const httpStatus = require("http-status");
const { pick } = require("lodash");
const AppError = require("../utils/AppError");
const { Product, Reservation } = require("../models");
const { getQueryOptions } = require("../utils/service.util");

const getProducts = async query => {
  const filter = pick(query, ["name", "category", "supplyNumber"]);
  const options = getQueryOptions(query);
  const products = await Product.find(filter, null, options);
  return products;
};

const getProductById = async productId => {
  const product = await Product.findById(productId);
  if (!product) {
    throw new AppError(httpStatus.NOT_FOUND, "Product not found");
  }
  return product;
};

const createProduct = async productBody => {
  const product = await Product.create(productBody);
  return product;
};

const updateProduct = async (productId, productBody) => {
  const product = await getProductById(productId);
  Object.assign(product, productBody);
  await product.save();
  return product;
};

const deleteProduct = async productId => {
  const product = await getProductById(productId);
  const reservations = await Reservation.find({ productId });
  const reservationIds = reservations.map(r => r._id);
  if (reservationIds.length !== 0) {
    throw new AppError(httpStatus.CONFLICT, "Product used in reservations. Can't delete", { reservationIds });
  }
  await product.remove();
  return product;
};

const canBeReserved = async (productId, amount) => {
  const product = await getProductById(productId);
  return product.currentAmount >= amount;
};

const reserve = async (productId, reserveAmount) => {
  const product = await getProductById(productId);
  const newAmount = product.currentAmount - reserveAmount;
  await updateProduct(productId, { currentAmount: newAmount });
};

const unreserv = async (productId, unreserveAmount) => {
  const product = await getProductById(productId);
  const newAmount = product.currentAmount + unreserveAmount;
  await updateProduct(productId, { currentAmount: newAmount });
};

const getAvailable = async requestedUserId => {
  const products = await Product.find({ currentAmount: { $gt: 0 } });
  return products;
};

module.exports = {
  getProducts,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
  canBeReserved,
  reserve,
  unreserv,
  getAvailable,
};
