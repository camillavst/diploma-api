const httpStatus = require("http-status");
const { pick } = require("lodash");
const path = require("path");
const MongooseArray = require("mongoose/lib/types/array");

const AppError = require("../utils/AppError");
const { Contract, Reservation } = require("../models");
const { getQueryOptions } = require("../utils/service.util");
const { customerService } = require(".");
const { templateToStream } = require("../utils/templateToStream");

const getContracts = async (query, user) => {
  const filter = pick(query, ["number"]);
  if (user.role === "user") {
    filter.managerId = user._id;
  }
  const options = getQueryOptions(query);
  const contracts = await Contract.find(filter, null, options);
  return contracts;
};

const getContractById = async (contractId, user) => {
  const filter = { _id: contractId };
  if (user.role === "user") {
    filter.managerId = user._id;
  }
  const contract = await Contract.findOne(filter);
  if (!contract) {
    throw new AppError(httpStatus.NOT_FOUND, "Contract not found");
  }
  return contract;
};

const getContractsByCustomerId = async contractId => {
  const filter = { contractId };
  const contracts = await Contract.find(filter);
  return contracts;
};

const tryLinkReservationToContract = async (reservationIds, contractId) => {
  const reservations = await Reservation.find({ _id: { $in: reservationIds } });
  const alreadyInContractIds = [];

  // eslint-disable-next-line no-restricted-syntax
  for (const res of reservations) {
    if (res.contractId) {
      alreadyInContractIds.push(res._id);
    }
  }
  if (alreadyInContractIds.length !== 0) {
    throw new AppError(httpStatus.CONFLICT, "Reservations already in contract. Can't add to contract", {
      alreadyInContractIds,
    });
  }
  // eslint-disable-next-line no-restricted-syntax
  for (const res of reservations) {
    res.contractId = contractId;
    // eslint-disable-next-line no-await-in-loop
    await res.save();
  }
};

const tryUnlinkReservationFromContract = async (reservationIds, contractId, user) => {
  await Reservation.updateMany({ contractId }, { $set: { contractId: null } });
};

const createContract = async (contractBody, user) => {
  const contractObj = contractBody;
  if (user && user._id) {
    contractObj.managerId = user._id.toString();
  }
  const contract = await Contract.create(contractObj);

  const { reservationIds } = contract;
  await tryLinkReservationToContract(reservationIds, contract._id, user);

  await contract.save();
  return contract;
};

const updateContract = async (contractId, contractBody, user) => {
  const contract = await getContractById(contractId, user);
  const oldContract = new Contract(contract);
  Object.assign(contract, contractBody);
  const oldResIds = oldContract.reservationIds.map(id => id.toString());
  const newIds = contract.reservationIds.map(id => id.toString());

  const excluded = oldResIds.filter(oldId => !newIds.includes(oldId));
  if (excluded.length !== 0) {
    const mongoIds = MongooseArray(excluded);
    await tryUnlinkReservationFromContract(mongoIds, contract._id);
  }

  const included = newIds.filter(newId => !oldResIds.includes(newId));
  if (included.length !== 0) {
    const mongoIds = MongooseArray(included);
    await tryLinkReservationToContract(mongoIds, contract._id);
  }
  await contract.save();
  return contract;
};

const deleteContract = async (contractId, user) => {
  const contract = await getContractById(contractId, user);
  if (contract.reservationIds.length !== 0) {
    await tryUnlinkReservationFromContract(contract.reservationIds, contract._id);
  }

  await contract.remove();
  return contract;
};

const formatContractForPrint = async contract => {
  const result = await contract.serialize();
  // format date
  const contractDate = contract.date;
  const year = contractDate.getFullYear();
  let month = contractDate.getMonth() + 1;
  const day = contractDate.getDate();
  if (month < 10) {
    month = `0${month}`;
  }
  result.date = `${year}.${month}.${day}`;

  return result;
};

const transformContractToPDF = async contract => {
  let template;
  try {
    template = await templateToStream(path.join(__dirname, "../templates/contract.ejs"), {
      contract: await formatContractForPrint(contract),
    });
    return template;
  } catch (e) {
    throw new AppError(httpStatus.BAD_REQUEST, "Can't render contract");
  }
};

const printContract = async (contractId, user) => {
  const contract = await getContractById(contractId, user);
  return transformContractToPDF(contract);
};

const getAvailableForUser = async requestedUserId => {
  const contracts = Contract.find({ managerId: requestedUserId });
  return contracts;
};

module.exports = {
  getContracts,
  getContractById,
  getContractsByCustomerId,
  createContract,
  updateContract,
  deleteContract,
  printContract,
  getAvailableForUser,
};
