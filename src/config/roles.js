const roles = ["user", "admin"];

const roleRights = new Map();
roleRights.set(roles[0], ["commonUser"]);
roleRights.set(roles[1], ["commonUser", "getUsers", "manageUsers"]);

module.exports = {
  roles,
  roleRights,
};
