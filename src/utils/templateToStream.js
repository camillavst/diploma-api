const ejs = require("ejs");
const pdf = require("html-pdf");

async function templateToStream(path, data) {
  return new Promise((resolve, reject) => {
    ejs.renderFile(path, data, (errEjs, rendered) => {
      if (errEjs) {
        return reject(errEjs);
      }
      pdf.create(rendered).toStream(function(errPdf, stream) {
        if (errPdf) {
          return reject(errPdf);
        }
        return resolve(stream);
      });
    });
  });
}

module.exports = {
  templateToStream,
};
